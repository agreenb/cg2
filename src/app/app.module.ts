import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './database/in-memory-db.service';

import { AppComponent } from './app.component';
import { TableComponent } from './components/table/table.component';
import { AppMaterialModule } from './modules/app-material.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatIconModule} from '@angular/material';
import { GridComponent } from './components/misc/grid/grid.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import {InfiniteScrollListComponent} from './components/infinite-scroll-list.component';

import 'hammerjs';
import { NagivationComponent } from './components/nagivation/nagivation.component';
import { CategoriesComponent } from './components/nagivation/categories/categories.component';
import { GenresComponent } from './components/nagivation/genres/genres.component';

@NgModule({
  declarations: [
    AppComponent,
    TableComponent,
    GridComponent,
    InfiniteScrollListComponent,
    NagivationComponent,
    CategoriesComponent,
    GenresComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppMaterialModule,
    BrowserAnimationsModule,
    MatIconModule,
    FlexLayoutModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(
      InMemoryDataService, { dataEncapsulation: false }
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
