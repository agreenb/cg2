import { Injectable } from '@angular/core';
import {Channel} from '../models/channel';
import {Observable, of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ChannelService {

  private channelsUrl = 'api/channels';

  getChannels(): Observable<Channel[]> {
    return this.http.get<Channel[]>(this.channelsUrl);
    // return of (CHANNELS);
  }

  constructor(private http: HttpClient) { }
}
