export class Pkg {
  name: string;
  key: string;
  price: number;
  subscription: string;
}
