import {SubGenre} from './sub-genre';

export class Genre {
  name: string;
  key: string;
  subGenres: SubGenre[];
}

