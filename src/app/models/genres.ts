import {Genre} from './genre';

export const GENRES: Genre[] = [
  {
    name : 'Music',
    key: 'music',
    subGenres: [
      {name: 'All Music', key: 'All Music'},
      {name: 'Rock', key: 'Rock'},
      {name: 'Pop', key: 'Pop'},
      {name: 'Country', key: 'Country'},
      {name: 'Hip-Hop/R&B', key: 'RandB_Hip-Hop'},
      {name: 'Christian', key: 'Christian'},
      {name: 'Jazz/Standards', key: 'Jazz/Standards'},
      {name: 'Classical', key: 'Classical'},
      {name: 'Dance/Electronic', key: 'Dance/Electronic'},
      {name: 'Latino', key: 'Latino'},
      {name: 'Holiday', key: 'Holiday'},
      {name: 'Canadian', key: 'Canadian'}
    ]
  }, {
    name : 'Sports',
    key: 'sports',
    subGenres: [
      {name: 'Sports', key: 'Sports'},
      {name: 'NFL Play-by-Play', key: 'NFL Play-by-Play'},
      {name: 'MLB Play-by-Play', key: 'MLB Play-by-Play'},
      {name: 'College Play-by-Play', key: 'College Play-by-Play'},
      {name: 'NBA Play-by-Play', key: 'NBA Play-by-Play'},
      {name: 'NHL Play-by-Play', key: 'NHL Play-by-Play'},
      {name: 'Sports Play-by-Play', key: 'Sports Play-by-Play'},
    ]
  }, {
    name : 'Talk & Entertainment',
    key: 'talk',
    subGenres: [
      {name: 'All Talk', key: 'talk'},
      {name: 'Entertainment', key: 'Entertainment'},
      {name: 'Comedy', key: 'Comedy'},
      {name: 'Religion', key: 'Religion'},
      {name: 'Kids', key: 'Kids'},
      {name: 'More', key: 'More'},
    ]
  }, {
    name : 'News & Issues',
    key: 'news',
    subGenres: [
      {name: 'All News', key: 'All News'},
      {name: 'News/Public Radio', key: 'News/Public Radio'},
      {name: 'Politics/Issues', key: 'Politics/Issues'},
      {name: 'Traffic/Weather', key: 'Traffic/Weather'},
    ]
  }, {
    name: 'Howard Stern',
    key: 'howard',
    subGenres: [{name: 'Howard Stern', key: 'howard'}],
  }, {
    name : 'All',
    key: 'all',
    subGenres: [{name: 'All', key: 'all'}],
  }
];
