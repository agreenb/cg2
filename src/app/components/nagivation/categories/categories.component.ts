import {Component, HostBinding, OnInit} from '@angular/core';
import {Genre} from '../../../models/genre';
import {GENRES} from '../../../models/genres';
import {MatTabChangeEvent} from '@angular/material';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {
  @HostBinding('@.disabled') disabled = true;
  genres: Genre[] = GENRES;
  selectedCategory = 0;

  updateCategory(event: MatTabChangeEvent) {
    console.log('updateGenre event: ', event);
    this.selectedCategory = event.index;
  }
  constructor() { }

  ngOnInit() {
  }

}
