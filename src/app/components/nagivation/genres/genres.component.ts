import {Component, Input, OnInit} from '@angular/core';
import {Genre} from '../../../models/genre';
import {MatTabChangeEvent} from '@angular/material';
import {SubGenre} from '../../../models/sub-genre';
import {GENRES} from '../../../models/genres';

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.scss']
})
export class GenresComponent implements OnInit {
  @Input() category: number;
  selectedGenre = 0;
  genres: SubGenre[];
  categories: Genre[] = GENRES;

  updateGenre(event: MatTabChangeEvent): void {
    console.log('sub tab ', event);
    this.selectedGenre = event.index;
  }

  constructor() { }

  ngOnInit() {
    this.genres = this.categories[this.category].subGenres;
  }

}
