import { AfterContentInit, Component, ViewChild } from '@angular/core';

import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import {MatGridList, MatGridTile} from '@angular/material';

@Component({
  selector: 'app-grid',
  templateUrl: './grid.component.html',
  styleUrls: ['./grid.component.scss']
})
export class GridComponent implements AfterContentInit {
  @ViewChild('grid') grid: MatGridList;
  @ViewChild('left') leftTile: MatGridTile;
  screenSize: string;

  gridByBreakpoint = {
    xl: 8,
    lg: 6,
    md: 4,
    sm: 2,
    xs: 1
  }
  rowByBreakpoint = {
    xl: '5:1',
    lg: '4:1',
    md: '3:1',
    sm: '2:1',
    xs: '2:1'
  }
  leftByBreakpoint = {
    lg: ['4']
  }


  constructor(private observableMedia: ObservableMedia) {}

  ngAfterContentInit() {
    this.observableMedia.asObservable().subscribe((change: MediaChange) => {
      this.grid.cols = this.gridByBreakpoint[change.mqAlias];
      this.grid.rowHeight = this.rowByBreakpoint[change.mqAlias];
      this.screenSize = change.mqAlias;
      this.leftTile.rowspan = this.leftByBreakpoint[change.mqAlias];
      console.log(this.screenSize);
    });

  }
}
