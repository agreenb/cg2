import {Component, Input, OnInit, HostBinding, AfterContentInit} from '@angular/core';
import {Pkg} from '../../models/pkg';
import {PKGS} from '../../models/pkgs';
import {GENRES} from '../../models/genres';
import {Genre} from '../../models/genre';
import {Channel} from '../../models/channel';
import { ChannelService } from '../../services/channel.service';
import {MatFormFieldAppearance, MatTabChangeEvent} from '@angular/material';
import {SubGenre} from '../../models/sub-genre';
import 'hammerjs';

import {fromEvent, Observable} from 'rxjs';
import {debounceTime} from 'rxjs/operators';



@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, AfterContentInit {
  @HostBinding('@.disabled') disabled = true;
  private SWIPE_ACTION: any;
  private BASE_VELOCITY: number;
  showFiller = false;

  swipe(idx, event) {
    const steps = this.calcSteps(event.velocityX);
    if (event.type === this.SWIPE_ACTION.LEFT) {
      const isLast = this.selectedGenre + steps >= this.genres.length - 1;
      this.selectedGenre = isLast ? this.genres.length - 1 : this.selectedGenre + steps;
    }

    if (event.type === this.SWIPE_ACTION.RIGHT) {
      const isFirst = this.selectedGenre - steps <= 0;
      this.selectedGenre = isFirst ? 0 : this.selectedGenre - steps;
    }
  }

  private calcSteps(velocity) {
    const v = Math.abs(velocity);
    if (v < 2 * this.BASE_VELOCITY) {
      return 1;
    } else if (v < 3 * this.BASE_VELOCITY) {
      return 2;
    } else if (v < 4 * this.BASE_VELOCITY) {
      return 3;
    } else {
      return 4;
    }
  }

  private itemHeight = 40;
  private numberOfItems = 10;

  numberBig: boolean = true
  numberSize(): void{
    if(this.numberBig){
      this.numberBig = false;
    } else {this.numberBig = true;}
  }

  title = 'Channel Guide';
  pkgs: Pkg[] = PKGS;
  genres: Genre[] = GENRES;
  channels: Channel[];
  selectedPkg: Pkg;
  breakpoint: number;
  searchOpen: boolean = false;

  public selectedGenre = 0;
  public selectedSubGenre = 0;


  table: object;
  // private modifyText: EventListenerOrEventListenerObject;

  openSearch(): void {
    if (this.searchOpen){
      this.searchOpen = false;
    } else {
      this.searchOpen = true;
    }
    console.log('searchOpen', this.searchOpen);
  }

  modifyText(): void {

    const t2 = document.getElementById('t2');
    console.log(t2.firstChild.nodeValue);
    if (t2.firstChild.nodeValue == 'three') {
      t2.firstChild.nodeValue = 'two';
    } else {
      t2.firstChild.nodeValue = 'three';
    }
  }



  infiniteScroll(): void {
    console.log(this.table);
  }

  updateGenre(event: MatTabChangeEvent) {
    console.log('updateGenre event: ', event);
    if (event.index !== 5) {
      this.selectedGenre = event.index;
      this.selectedSubGenre = 0;
      this.getChannels(this.genres[this.selectedGenre].subGenres[this.selectedSubGenre]);
    }
  }


  updateSubGenre(event: MatTabChangeEvent) {
    if (this.selectedGenre !== 6) {
      this.selectedSubGenre = event.index;
      this.getChannels(this.genres[this.selectedGenre].subGenres[this.selectedSubGenre]);
      console.log('event: ', event);
    }
  }


  getChannels(subGenre: SubGenre): void {
    if (this.selectedSubGenre === 0) {
      this.channelService.getChannels()
        .subscribe(channels => this.channels = channels.filter(channel => channel.progtypetitle === this.genres[this.selectedGenre].name));
    } else {
      this.channelService.getChannels()
        .subscribe(channels => this.channels = channels.filter(channel => channel.genretitle === subGenre.name));
    }
  }





  constructor(private channelService: ChannelService) {  }


  ngOnInit() {
    this.getChannels(this.genres[this.selectedGenre].subGenres[this.selectedSubGenre]);
    this.selectedPkg = PKGS[0];
    this.breakpoint = (window.innerWidth <= 400) ? 2 : 5;
    this.infiniteScroll();


  }

  ngAfterContentInit(): void {
    this.infiniteScroll();

    const el = document.getElementById('outside');
    // el.addEventListener('click', this.modifyText);

    // const table = document.getElementById('fixed_header');
    // table.addEventListener('wheel', this.infiniteScroll);
  }
}
